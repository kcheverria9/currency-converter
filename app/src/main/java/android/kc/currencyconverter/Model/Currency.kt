package android.kc.currencyconverter.Model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class Currency(var country: String = "", var buy: Float = 0.0f, var sell: Float = 0.0f, var recentDate: String = "2018-1-5", var websiteDate: String = "2018-1-5") :RealmObject(), Serializable {
    @PrimaryKey var id: Int = 1

    constructor(currency: Currency) : this(currency.country, currency.buy, currency.sell, currency.recentDate, currency.websiteDate)

    constructor(id: Int, country: String, buy: Float, sell: Float, recentDate: String, websiteDate: String) : this(country, buy, sell, recentDate, websiteDate) {
        this.id = id
    }
}

