package android.kc.currencyconverter.Helper

import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.RecyclerViewAdapters.FavoritesRatesAdapter
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


class BindingUtil {

    companion object {

        @BindingAdapter("bind:selectedRates", "layoutId")
        @JvmStatic
        fun bindSelectedRates(view: RecyclerView, list: ObservableArrayList<Currency>, layout: Int) {
            val layoutManager = LinearLayoutManager(view.context,LinearLayoutManager.HORIZONTAL,false)
            view.layoutManager = layoutManager
            view.adapter = FavoritesRatesAdapter(view, list, layout)
        }


        @BindingAdapter("bind:unselectedRates", "layoutId")
        @JvmStatic
        fun bindUnselectedRates(view: RecyclerView, list: ObservableArrayList<Currency>, layout: Int) {
            val layoutManager = GridLayoutManager(view.context, 3)
            view.layoutManager = layoutManager
            view.adapter = FavoritesRatesAdapter(view, list, layout)
        }
    }

}