package android.kc.currencyconverter.Helper


import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.kc.currencyconverter.Model.Currency

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
@SuppressLint("Recycle")
class MySQLiteHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    val allRates: ArrayList<Currency>
        @SuppressLint("SimpleDateFormat")
        get() {
            val currencies = ArrayList<Currency>()

            val query = "SELECT *FROM " + TABLE_ALL_RATES

            val db = this.writableDatabase
            val cursor = db.rawQuery(query, null)
            var currency: Currency

            if (cursor.moveToFirst()) {
                do {
                    try {
                        val recentDate = SimpleDateFormat("yyyy-MM-dd").parse(cursor.getString(4))
                        val websiteDate = SimpleDateFormat("yyyy-MM-dd").parse(cursor.getString(5))
                        currency = Currency(Integer.parseInt(cursor.getString(0)), cursor.getString(1), java.lang.Float.parseFloat(cursor.getString(2)), java.lang.Float.parseFloat(cursor.getString(3)), SimpleDateFormat("yyyy-MM-dd").format(recentDate), SimpleDateFormat("yyyy-MM-dd").format(websiteDate))
                        currencies.add(Currency(currency))
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

                } while (cursor.moveToNext())
            }

            return currencies
        }

val favRates: ArrayList<Currency>
        @SuppressLint("SimpleDateFormat")
        get() {
            val currencies = ArrayList<Currency>()

            val query = "SELECT *FROM " + TABLE_FAVORITE_RATES

            val db = this.writableDatabase
            val cursor = db.rawQuery(query, null)
            var currency: Currency

            if (cursor.moveToFirst()) {
                do {
                    try {
                        val recentDate = SimpleDateFormat("yyyy-MM-dd").parse(cursor.getString(4))
                        val websiteDate = SimpleDateFormat("yyyy-MM-dd").parse(cursor.getString(5))
                        currency = Currency(Integer.parseInt(cursor.getString(0)), cursor.getString(1), java.lang.Float.parseFloat(cursor.getString(2)), java.lang.Float.parseFloat(cursor.getString(3)), SimpleDateFormat("yyyy-MM-dd").format(recentDate), SimpleDateFormat("yyyy-MM-dd").format(websiteDate))
                        currencies.add(Currency(currency))
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

                } while (cursor.moveToNext())
            }

            return currencies
        }

    val mostRecentDate: String
        @SuppressLint("SimpleDateFormat")
        get() {
            val db = this.writableDatabase
            val rates = db.rawQuery("SELECT $MOST_RECENT_DATE FROM $TABLE_ALL_RATES LIMIT 1", null)
            var date = ""
            if (rates.moveToFirst()) {
                date = rates.getString(0)
            }

            return date
        }
    val mostRecentWebsiteDate: String
        @SuppressLint("SimpleDateFormat")
        get() {
            val db = this.writableDatabase
            val rates = db.rawQuery("SELECT $MOST_RECENT_WEBSITE_DATE FROM $TABLE_ALL_RATES LIMIT 1", null)
            var date = ""
            if (rates.moveToFirst()) {
                date = rates.getString(0)
            }

            return date
        }


    val topMostRecentDate: String
        @SuppressLint("SimpleDateFormat")
        get() {
            val db = this.writableDatabase
            val topRates = db.rawQuery("SELECT $MOST_RECENT_DATE FROM $TABLE_TOP_RATES LIMIT 1", null)
            var date = ""
            if (topRates.moveToFirst()) {
                date = topRates.getString(0)
            }

            return date
        }
    val topMostRecentWebsiteDate: String
        get() {
            val db = this.writableDatabase
            val topRates = db.rawQuery("SELECT $MOST_RECENT_WEBSITE_DATE FROM $TABLE_TOP_RATES LIMIT 1", null)
            var date = ""
            if (topRates.moveToFirst()) {
                date = topRates.getString(0)
            }

            return date
        }

    val allTopRates: ArrayList<Currency>
        @SuppressLint("SimpleDateFormat")
        get() {
            val currencies = ArrayList<Currency>()

            val query = "SELECT *FROM " + TABLE_TOP_RATES

            val db = this.writableDatabase
            val cursor = db.rawQuery(query, null)
            var currency: Currency

            if (cursor.moveToFirst()) {
                do {
                    try {
                        val recentDate = SimpleDateFormat("yyyy-MM-dd").parse(cursor.getString(4))
                        val websiteDate = SimpleDateFormat("yyyy-MM-dd").parse(cursor.getString(5))
                        currency = Currency(Integer.parseInt(cursor.getString(0)), cursor.getString(1), java.lang.Float.parseFloat(cursor.getString(2)), java.lang.Float.parseFloat(cursor.getString(3)), SimpleDateFormat("yyyy-MM-dd").format(recentDate), SimpleDateFormat("yyyy-MM-dd").format(websiteDate))
                        currencies.add(Currency(currency))
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

                } while (cursor.moveToNext())
            }

            return currencies
        }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        val CREATE_ALL_RATES_TABLE = "CREATE TABLE all_rates (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "country TEXT," +
                "buy REAL," +
                "sell REAL, " +
                "recent_date TEXT, " +
                "website_date TEXT )"

        val CREATE_TOP_RATES_TABLE = "CREATE TABLE top_rates (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "country TEXT," +
                "buy REAL," +
                "sell REAL, " +
                "recent_date TEXT, " +
                "website_date TEXT )"

        val CREATE_FAVORITES_RATES_TABLE = "CREATE TABLE favorites_rates (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "country TEXT," +
                "buy REAL," +
                "sell REAL, " +
                "recent_date TEXT, " +
                "website_date TEXT )"

        sqLiteDatabase.execSQL(CREATE_ALL_RATES_TABLE)
        sqLiteDatabase.execSQL(CREATE_TOP_RATES_TABLE)
        sqLiteDatabase.execSQL(CREATE_FAVORITES_RATES_TABLE)
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

        if (newVersion > oldVersion) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS all_rates")
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS top_rates")
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS favorites_rates")
            this.onCreate(sqLiteDatabase)
        }
    }

    fun addFavorite(currency: Currency){
        val db = this.writableDatabase

        val values = ContentValues()

        values.put(KEY_COUNTRY, currency.country)
        values.put(KEY_BUY, currency.buy)
        values.put(KEY_SELL, currency.sell)
        values.put(MOST_RECENT_DATE, currency.recentDate)
        values.put(MOST_RECENT_WEBSITE_DATE, currency.websiteDate)
        db.insert(TABLE_FAVORITE_RATES, null, values)
        db.close()
    }

    fun addAllRate(currency: Currency) {
        val db = this.writableDatabase

        val values = ContentValues()

        values.put(KEY_COUNTRY, currency.country)
        values.put(KEY_BUY, currency.buy)
        values.put(KEY_SELL, currency.sell)
        values.put(MOST_RECENT_DATE, currency.recentDate)
        values.put(MOST_RECENT_WEBSITE_DATE, currency.websiteDate)
        db.insert(TABLE_ALL_RATES, null, values)
        db.close()
    }

    fun clearTopRates() {
        this.writableDatabase.execSQL("DELETE FROM " + TABLE_TOP_RATES)
    }

    fun clearAllRates() {
        this.writableDatabase.execSQL("DELETE FROM " + TABLE_ALL_RATES)
    }

    fun clearFavorites(){
        this.writableDatabase.execSQL("DELETE FROM "+ TABLE_FAVORITE_RATES)
    }

    fun addTopRate(currency: Currency) {
        val db = this.writableDatabase

        val values = ContentValues()

        values.put(KEY_COUNTRY, currency.country)
        values.put(KEY_BUY, currency.buy)
        values.put(KEY_SELL, currency.sell)
        values.put(MOST_RECENT_DATE, currency.recentDate)
        values.put(MOST_RECENT_WEBSITE_DATE, currency.websiteDate)
        db.insert(TABLE_TOP_RATES, null, values)
        db.close()
    }

    companion object {

        private val DATABASE_VERSION = 7

        private val DATABASE_NAME = "RatesDB"
        private val TABLE_ALL_RATES = "all_rates"
        private val TABLE_TOP_RATES = "top_rates"
        private val KEY_ID = "id"
        private val KEY_COUNTRY = "country"
        private val KEY_BUY = "buy"
        private val KEY_SELL = "sell"
        private val MOST_RECENT_DATE = "recent_date"
        private val MOST_RECENT_WEBSITE_DATE = "website_date"
        private val TABLE_FAVORITE_RATES = "favorites_rates"
    }
}
