package android.kc.currencyconverter.Services

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.kc.currencyconverter.Helper.MySQLiteHelper
//import android.kc.currencyconverter.Model.AllRates
import android.kc.currencyconverter.Model.Currency
//import android.kc.currencyconverter.Model.TopRates
import android.os.*
import android.support.v4.app.NotificationCompat
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class RatesService : Service() {

    private lateinit var looper: Looper
    private lateinit var handler: ServiceHandler
    private var builder: NotificationCompat.Builder = NotificationCompat.Builder(this)
    private lateinit var manager: NotificationManager


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        val thread = HandlerThread("RatesService", Process.THREAD_PRIORITY_BACKGROUND)
        manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        builder = NotificationCompat.Builder(this)
        builder.setContentTitle("Currency Converter")
                .setContentText("Downloading rates")
                .setSmallIcon(android.R.drawable.stat_sys_download)

        thread.start()
        builder.setProgress(0, 0, true)
        manager.notify(1, builder.build())
        looper = thread.looper
        handler = ServiceHandler(looper)

    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        val message = handler.obtainMessage()
        message.arg1 = startId
        handler.sendMessage(message)
        return Service.START_NOT_STICKY
    }

    fun getAllRates() {
        var document: Document? = null
        val db = MySQLiteHelper(applicationContext)
        try {
            val response = Jsoup.connect("http://www.boj.org.jm/foreign_exchange/fx_irates.php").timeout(10000).execute()
            val htmlDocument = response.body()
            document = Jsoup.parse(htmlDocument)
            if (document != null) {
                val date = document.getElementsByTag("strong")[0].text()
                if (db.mostRecentWebsiteDate != date) {
                    val tableBody = document.getElementsByTag("tbody")[0]
                    db.clearAllRates()
                    val currencyRow = tableBody.getElementsByTag("tr")
                    for (row in currencyRow) {
                        val cells = row.getElementsByTag("td")
                        val buy = cells[1].text().replace("[^\\d.]", "")
                        val sell = cells[2].text().replace("[^\\d.]", "")

                        @SuppressLint("SimpleDateFormat") val recentDate = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)
                        val currency = Currency(cells[0].text(), java.lang.Float.parseFloat(buy), java.lang.Float.parseFloat(sell), recentDate, date)
                        db.addAllRate(currency)
                    }
                }

            }
        } catch (io: IOException) {
            io.printStackTrace()
        }

    }

    fun getTopRates() {
        var document: Document? = null
        val db = MySQLiteHelper(applicationContext)
        try {
            val response = Jsoup.connect("http://www.boj.org.jm/foreign_exchange/fx_crates.php").timeout(10000).execute()
            val htmlDocument = response.body()
            document = Jsoup.parse(htmlDocument)
            if (document != null) {
                val date = document.getElementsByTag("b")[1].text()
                if (db.topMostRecentWebsiteDate != date) {
                    val tableBody = document.getElementsByTag("tbody")[0]
                    db.clearTopRates()
                    val currencyRow = tableBody.getElementsByTag("tr")
                    for (row in currencyRow) {
                        val cells = row.getElementsByTag("td")
                        val buy = cells[1].text().replace("[^\\d.]", "")
                        val sell = cells[4].text().replace("[^\\d.]", "")
                        @SuppressLint("SimpleDateFormat") val recentDate = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)
                        val currency = Currency(cells[0].text(), java.lang.Float.parseFloat(buy), java.lang.Float.parseFloat(sell), recentDate, date)
                        db.addTopRate(currency)
                    }
                }
            }
        } catch (io: IOException) {
            io.printStackTrace()
        }

    }

    override fun onDestroy() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        alarmManager.set(AlarmManager.RTC_WAKEUP, AlarmManager.INTERVAL_HALF_DAY, PendingIntent.getService(this, 0, Intent(this, RatesService::class.java), 0))
    }

    private inner class ServiceHandler(looper: Looper?) : Handler(looper) {

        override fun handleMessage(msg: Message) {

            try {
                Thread.sleep(5000)
            } catch (e: InterruptedException) {
                Thread.currentThread().interrupt()
            }
            getTopRates()
            getAllRates()
        }
    }
}
