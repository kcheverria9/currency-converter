package android.kc.currencyconverter.Fragments

import android.app.AlertDialog
import android.app.Dialog
import android.databinding.DataBindingUtil
import android.databinding.ObservableArrayList
import android.kc.currencyconverter.Helper.MySQLiteHelper
import android.kc.currencyconverter.Interfaces.FavoritesSaveListener
import android.kc.currencyconverter.KotlinExtensions.removeFavorites
import android.kc.currencyconverter.KotlinExtensions.toObservableArrayList
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.R
import android.kc.currencyconverter.RatesActivity
import android.kc.currencyconverter.databinding.FavoritesBinding
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.MotionEvent
import android.widget.Toast

class FavoritesDialog : DialogFragment() {

    var favoriteRates: ObservableArrayList<Currency> = ObservableArrayList()
    var allRates: ObservableArrayList<Currency> = ObservableArrayList()
    lateinit var binding: FavoritesBinding
    lateinit var listener: FavoritesSaveListener

    companion object {
        fun newInstance(listener: FavoritesSaveListener): FavoritesDialog {
            val favoritesDialog = FavoritesDialog()
            favoritesDialog.listener = listener
            return favoritesDialog
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        favoriteRates = RatesActivity.favRates.toObservableArrayList()

        allRates = RatesActivity.favRates.removeFavorites(RatesActivity.topCurrencies)

        val inflater = LayoutInflater.from(activity) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_favorites, null, false)
        binding.favoritesDialog = this
        binding.selectedRates.addOnItemTouchListener(SelectedTouchListener())
        binding.unselectedRates.addOnItemTouchListener(OnTouchListener())
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setView(binding.root)

        builder.setPositiveButton("Save", null)

        builder.setNegativeButton("Cancel", { dialogInterface, i ->
            dialogInterface.dismiss()
        })

        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.setOnShowListener({ dialogInterface ->
            val button = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener {
                if (favoriteRates.size < 4) {
                    Toast.makeText(activity?.applicationContext, "Please select 4 favorite currencies", Toast.LENGTH_SHORT).show()
                } else {
                    val db = MySQLiteHelper(this.activity?.applicationContext!!)
                    db.clearFavorites()
                    for (currency in favoriteRates) {
                        db.addFavorite(currency)
                    }
                    RatesActivity.favRates = favoriteRates
                    listener.saveFavorites(favoriteRates)
                    dialogInterface.dismiss()
                }
            }
        })
        return dialog
    }


    inner class SelectedTouchListener : RecyclerView.OnItemTouchListener {
        val gestureDetector = GestureDetector(this@FavoritesDialog.context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapUp(e: MotionEvent): Boolean {
                return true
            }
        })

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {

        }

        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val childView = rv.findChildViewUnder(e.x, e.y)
            if (childView != null && gestureDetector.onTouchEvent(e)) {
                val position = rv.getChildAdapterPosition(childView)
                val currency = favoriteRates[position]
                if(favoriteRates.size>1){
                    allRates.add(currency)
                    favoriteRates.remove(currency)
                    binding.unselectedRates.adapter.notifyDataSetChanged()
                    binding.executePendingBindings()
                }else{
                    Toast.makeText(activity?.applicationContext,"Need at least 1 favorite",Toast.LENGTH_SHORT).show()
                }
            }
            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

        }
    }

    inner class OnTouchListener : RecyclerView.OnItemTouchListener {
        val gestureDetector = GestureDetector(this@FavoritesDialog.context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapUp(e: MotionEvent): Boolean {
                return true
            }
        })

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {

        }

        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val childView = rv.findChildViewUnder(e.x, e.y)
            if (childView != null && gestureDetector.onTouchEvent(e)) {
                val position = rv.getChildAdapterPosition(childView)
                val currency = allRates[position]
                if (favoriteRates.size < 4) {
                    favoriteRates.add(currency)
                    allRates.remove(currency)
                    binding.unselectedRates.adapter.notifyItemChanged(position)
                    binding.executePendingBindings()
                } else {
                    Toast.makeText(activity?.applicationContext, "Can only have 4 favorites", Toast.LENGTH_SHORT).show()
                }
            }
            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

        }
    }

}

