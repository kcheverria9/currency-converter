package android.kc.currencyconverter.Fragments


import `in`.galaxyofandroid.spinerdialog.SpinnerDialog
import android.kc.currencyconverter.KotlinExtensions.findByName
import android.kc.currencyconverter.R
import android.kc.currencyconverter.RatesActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast


/**
 * A simple [Fragment] subclass.
 */
class SellingFragment : Fragment() {

    private val allCountries: ArrayList<String> = ArrayList()
    private var allCurrencies = RatesActivity.currencies

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_selling, container, false)

        val buttonCurrency = view.findViewById<Button>(R.id.select_currency_selling)
        val currencySelling = view.findViewById<TextView>(R.id.currency_selling)
        val conversionSelling = view.findViewById<TextView>(R.id.conversion_selling)
        val money = view.findViewById<EditText>(R.id.money_sell)

        money.setOnFocusChangeListener({v, hasFocus ->
            if(hasFocus){
                money.setText("")
            }
        })

        allCurrencies.mapTo(allCountries) { it.country }

        val spinnerDialog = SpinnerDialog(this.activity, allCountries, "Select or Search Currency")

        currencySelling.text = if (allCountries.isNotEmpty()) allCountries[0] else ""
        conversionSelling.text = String.format("$%.3f", convertSelling(currencySelling.text.toString(), money.text.toString().toFloat()))


        spinnerDialog.bindOnSpinerListener { item, position ->
            currencySelling.text = item
            if (money.text.isNotEmpty()) {
                conversionSelling.text = String.format("$%.3f JMD", convertSelling(item, money.text.toString().toFloat()))
            }
        }


        buttonCurrency.setOnClickListener({ spinnerDialog.showSpinerDialog() })

        money.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!(currencySelling.text.isEmpty())) {
                    if (!(p0.isNullOrEmpty())) {
                        if(p0?.startsWith('.')!!){
                            p0.insert(0,"0")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            conversionSelling.text = String.format("$%.3f", convertSelling(currencySelling.text.toString(), money.text.toString().toFloat()))
                        }
                    } else {
                        conversionSelling.text = "0.00 JMD"
                    }
                } else {
                    Toast.makeText(this@SellingFragment.context, "Please select a currency", Toast.LENGTH_SHORT).show()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!(currencySelling.text.isEmpty())) {
                    if (!(p0.isNullOrEmpty())) {
                        if(p0?.startsWith('.')!!){
                            p0.replaceFirst(Regex("[.]"),"0.")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            conversionSelling.text = String.format("$%.3f", convertSelling(currencySelling.text.toString(), money.text.toString().toFloat()))
                        }
                    } else {
                        conversionSelling.text = "0.00 JMD"
                    }
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!(currencySelling.text.isEmpty())) {
                    if (!(p0.isNullOrEmpty())) {
                        if(p0?.startsWith('.')!!){
                            p0.replaceFirst(Regex("[.]"),"0.")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            conversionSelling.text = String.format("$%.3f", convertSelling(currencySelling.text.toString(), money.text.toString().toFloat()))
                        }
                    } else {
                        conversionSelling.text = "0.00 JMD"
                    }
                } else {
                    Toast.makeText(this@SellingFragment.context, "Please select a currency", Toast.LENGTH_SHORT).show()
                }
            }
        })

        return view
    }

    fun convertSelling(country: String, money: Float): Float {
        if (country.isNotEmpty()) {
            val currency = allCurrencies.findByName(country)
            return (money * currency.sell)
        }
        return 0.0f
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        val item = menu?.findItem(R.id.favorites)
        item?.isVisible = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

}// Required empty public constructor
