package android.kc.currencyconverter.Fragments

import android.app.AlertDialog
import android.app.Dialog
import android.kc.currencyconverter.Helper.MySQLiteHelper
import android.kc.currencyconverter.RecyclerViewAdapters.SaveListAdapter
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.R
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater

class SelectFavoritesDialog : DialogFragment() {

    var favoriteCurrencies: ArrayList<Currency> = FavoritesFragment.favRates


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(activity) as LayoutInflater
        val view = inflater.inflate(R.layout.save_favorites_layout, null, false)
        val recyclerView = view.findViewById(R.id.save_list) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = SaveListAdapter(favoriteCurrencies)

        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setView(view)
        builder.setPositiveButton("Save", { dialogInterface, i ->
            val db = MySQLiteHelper(this.activity?.applicationContext!!)
            db.clearFavorites()
            for (currency in favoriteCurrencies) {
                db.addFavorite(currency)
            }
        })


        builder.setCancelable(false)
        return builder.create()
    }

}