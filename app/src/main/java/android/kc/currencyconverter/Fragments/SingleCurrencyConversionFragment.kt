package android.kc.currencyconverter.Fragments


import android.annotation.SuppressLint
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.R
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView


/**
 * A simple [Fragment] subclass.
 */
class SingleCurrencyConversionFragment : Fragment() {


    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_single_currency_conversion, container, false)
        val currency = arguments?.getSerializable("currency") as Currency
        val mainCurrency = view.findViewById<TextView>(R.id.main_currency)
        val secondaryCurrency = view.findViewById<TextView>(R.id.secondary_currency)
        val swapCurrency = view.findViewById<ImageButton>(R.id.swap)
        val currencyConversion = view.findViewById<TextView>(R.id.currency_conversion)
        val money = view.findViewById<EditText>(R.id.money)

        val conversion = (money.text.toString().toFloat() / currency.buy)
        currencyConversion.text = String.format("$%.3f", conversion)

        money.setOnFocusChangeListener({v, hasFocus ->
            if(hasFocus){
                money.setText("")
            }
        })


        var swapClicked = false
        swapCurrency.isClickable = false
        mainCurrency.text = getString(R.string.JMD).toUpperCase()
        secondaryCurrency.text = currency.country



        swapCurrency.setOnClickListener({
            val main = mainCurrency.text
            val secondary = secondaryCurrency.text
            val text = money.text
            var conversion = ""
            conversion = if (currencyConversion.text.toString().isNotEmpty()) currencyConversion.text.toString().parseCurrency() else "0"
            if (swapClicked) {
                swapClicked = false
                mainCurrency.text = secondary
                secondaryCurrency.text = main
                money.setText(String.format("%.3f", if (conversion.isNotEmpty()) conversion.toFloat() else 0.00))
                currencyConversion.text = String.format("%.3f", if (text.isNotEmpty()) text.toString().toFloat() else 0.00)
            } else {
                swapClicked = true
                mainCurrency.text = secondary
                secondaryCurrency.text = main
                money.setText(String.format("%.3f", if (conversion.isNotEmpty()) conversion.toFloat() else 0.00))
                currencyConversion.text = String.format("%.3f", if (text.isNotEmpty()) text.toString().toFloat() else 0.00)
            }

        })

        money.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!(p0.isNullOrEmpty())) {
                    if (!(swapClicked)) {
                        if(p0?.startsWith('.')!!){
                            p0.insert(0,"0")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            val conversion = (p0.toString().toFloat() / currency.buy)
                            currencyConversion.text = String.format("$%.3f", conversion)
                        }
                    } else {
                        if(p0?.startsWith('.')!!){
                            p0.insert(0,"0")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            val conversion = (p0.toString().toFloat() * currency.sell)
                            currencyConversion.text = String.format("$%.3f JMD", conversion)
                        }
                    }
                } else {
                    currencyConversion.text = "0.00"
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!(p0.isNullOrEmpty())) {
                    if (!(swapClicked)) {

                        if(p0?.startsWith('.')!!){
                            p0.replaceFirst(Regex("[.]"),"0.")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            val conversion = (p0.toString().toFloat() / currency.sell)
                            currencyConversion.text = String.format("$%.3f", conversion)
                        }
                    } else {
                        if(p0?.startsWith('.')!!){
                            p0.replaceFirst(Regex("[.]"),"0.")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            val conversion = (p0.toString().toFloat() * currency.sell)
                            currencyConversion.text = String.format("$%.3f JMD", conversion)
                        }
                    }
                } else {
                    currencyConversion.text = "0.00"
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!(p0.isNullOrEmpty())) {
                    if (!(swapClicked)) {

                        if(p0?.startsWith('.')!!){
                            p0.replaceFirst(Regex("[.]"),"0.")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            val conversion = (p0.toString().toFloat() / currency.buy)
                            currencyConversion.text = String.format("$%.3f", conversion)
                        }
                    } else {
                        if(p0?.startsWith('.')!!){
                            p0.replaceFirst(Regex("[.]"),"0.")
                        }
                        if(p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))){
                            val conversion = (p0.toString().toFloat() * currency.sell)
                            currencyConversion.text = String.format("$%.3f JMD", conversion)
                        }

                    }
                } else {
                    currencyConversion.text = "0.00"
                }
            }
        })
        return view
    }

    private fun String.parseCurrency(): String {
        var string = ""
        if (this.isNotEmpty()) {
            string = this.removePrefix("$")
            if (this.contains("JMD")) {
                string = string.removeSurrounding("", " JMD")
            }
        }
        return string
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        val item = menu?.findItem(R.id.favorites)
        item?.isVisible = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

}// Required empty public constructor
