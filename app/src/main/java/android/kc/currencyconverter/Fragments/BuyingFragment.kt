package android.kc.currencyconverter.Fragments

import `in`.galaxyofandroid.spinerdialog.SpinnerDialog
import android.kc.currencyconverter.KotlinExtensions.findByName
import android.kc.currencyconverter.R
import android.kc.currencyconverter.RatesActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast


class BuyingFragment : Fragment() {

    private val allCountries: ArrayList<String> = ArrayList()
    private var allCurrencies = RatesActivity.currencies

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_buying, container, false)

        val buttonCurrency = view.findViewById<Button>(R.id.select_currency_buying)
        val currencyBuying = view.findViewById<TextView>(R.id.currency_buy)
        val conversionBuying = view.findViewById<TextView>(R.id.conversion_buy)
        val money = view.findViewById<EditText>(R.id.money_buy)
        allCurrencies.mapTo(allCountries) { it.country }
        money.setOnFocusChangeListener({v, hasFocus ->
            if(hasFocus){
                money.setText("")
            }
        })

        val spinnerDialog = SpinnerDialog(this.activity, allCountries, "Select or Search Currency")

        currencyBuying.text = if (allCountries.isNotEmpty()) allCountries[0] else ""
        conversionBuying.text = String.format("$%.3f", convertBuying(currencyBuying.text.toString(), money.text.toString().toFloat()))

        spinnerDialog.bindOnSpinerListener { item, position ->
            currencyBuying.text = item
            if (money.text.isNotEmpty()) {
                conversionBuying.text = String.format("$%.3f", convertBuying(item, money.text.toString().toFloat()))
            }
        }

        buttonCurrency.setOnClickListener({ spinnerDialog.showSpinerDialog() })

        money.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!(currencyBuying.text.isEmpty())) {
                    if (!(p0.isNullOrEmpty())) {

                        if (p0?.startsWith('.')!!) {
                            p0.insert(0, "0")
                        }
                        if (p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))) {
                            conversionBuying.text = String.format("$%.3f", convertBuying(currencyBuying.text.toString(), money.text.toString().toFloat()))
                        }
                    } else {
                        conversionBuying.text = "0.00"
                    }
                } else {
                    Toast.makeText(this@BuyingFragment.context, "Please select a currency", Toast.LENGTH_SHORT).show()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!(currencyBuying.text.isEmpty())) {
                    if (!(p0.isNullOrEmpty())) {

                        if (p0?.startsWith('.')!!) {
                            p0.replaceFirst(Regex("[.]"), "0.")
                        }
                        if (p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))) {
                            conversionBuying.text = String.format("$%.3f", convertBuying(currencyBuying.text.toString(), money.text.toString().toFloat()))
                        }

                    } else {
                        conversionBuying.text = "0.00"
                    }
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!(currencyBuying.text.isEmpty())) {
                    if (!(p0.isNullOrEmpty())) {

                        if (p0?.startsWith('.')!!) {
                            p0.replaceFirst(Regex("[.]"), "0.")
                        }
                        if (p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))) {
                            conversionBuying.text = String.format("$%.3f", convertBuying(currencyBuying.text.toString(), money.text.toString().toFloat()))
                        }
                    } else {
                        conversionBuying.text = "0.00"
                    }
                } else {
                    Toast.makeText(this@BuyingFragment.context, "Please select a currency", Toast.LENGTH_SHORT).show()
                }
            }
        })

        return view
    }


    fun convertBuying(country: String, money: Float): Float {
        if (country.isNotEmpty()) {
            val currency = allCurrencies.findByName(country)
            return money / currency.buy
        }
        return 0.0f
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        val item = menu?.findItem(R.id.favorites)
        item?.isVisible = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }
}

