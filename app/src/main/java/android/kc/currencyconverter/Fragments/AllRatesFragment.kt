package android.kc.currencyconverter.Fragments


import android.kc.currencyconverter.RecyclerViewAdapters.CountryAdapter
import android.kc.currencyconverter.Interfaces.CurrencyClickListener
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.R
import android.kc.currencyconverter.RatesActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.app_bar_rates.*


/**
 * A simple [Fragment] subclass.
 */
class AllRatesFragment : Fragment(), CurrencyClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_all_rates, container, false)

        val recyclerView = view.findViewById<RecyclerView>(R.id.all_rates_recyclerview)
        val rates = RatesActivity.currencies
        val date = view.findViewById<TextView>(R.id.date)
        date.text = if(rates.isNotEmpty()) "Rates as of: ${rates[0].websiteDate} for JMD" else ""
        recyclerView.layoutManager = GridLayoutManager(this.context, 2)
        recyclerView.adapter = CountryAdapter(rates, this)
        return view
    }

    override fun onCurrencyClicked(currency: Currency) {
        val transaction = fragmentManager?.beginTransaction()
        activity?.toolbar?.title = "Rates"
        val bundle = Bundle()
        bundle.putSerializable("currency", currency)
        val singleCurrency = SingleCurrencyConversionFragment()
        singleCurrency.arguments = bundle
        transaction?.addToBackStack("conversion")
        transaction?.hide(this)
        transaction?.add(R.id.frame_layout, singleCurrency)
        transaction?.commit()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        val item = menu?.findItem(R.id.favorites)
        item?.isVisible = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

}
