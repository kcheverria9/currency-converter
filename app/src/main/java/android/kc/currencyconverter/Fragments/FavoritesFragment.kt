package android.kc.currencyconverter.Fragments


import android.kc.currencyconverter.Interfaces.FavoritesSaveListener
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.R
import android.kc.currencyconverter.RatesActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import android.widget.TextView


class FavoritesFragment : Fragment(), FavoritesSaveListener, TextWatcher {


    companion object {
        var favRates: ArrayList<Currency> = ArrayList()
        var topCurrencies: ArrayList<Currency> = ArrayList()
    }

    private lateinit var countryOne: TextView
    private lateinit var countryTwo: TextView
    private lateinit var countryThree: TextView
    private lateinit var countryFour: TextView
    private lateinit var conversionOne: TextView
    private lateinit var conversionTwo: TextView
    private lateinit var conversionThree: TextView
    private lateinit var conversionFour: TextView
    private lateinit var input: EditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_favorites, container, false)

        input = view.findViewById(R.id.input)
        topCurrencies = RatesActivity.topCurrencies
        favRates = RatesActivity.favRates
        if (favRates.isEmpty()) {
            val fragment = FavoritesDialog.newInstance(this)
            fragment.show(fragmentManager, "favorites")
        }

        input.addTextChangedListener(this)
        countryOne = view.findViewById(R.id.country_1)
        countryTwo = view.findViewById(R.id.country_2)
        countryThree = view.findViewById(R.id.country_3)
        countryFour = view.findViewById(R.id.country_4)

        conversionOne = view.findViewById(R.id.conversion_1)
        conversionTwo = view.findViewById(R.id.conversion_2)
        conversionThree = view.findViewById(R.id.conversion_3)
        conversionFour = view.findViewById(R.id.conversion_4)
        setTextViews()
        input.setOnFocusChangeListener({ v, hasFocus ->
            if (hasFocus) {
                input.setText("")
            }
        })
        calculateExchange(input.text.toString().toFloat())
        return view
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        val item = menu?.findItem(R.id.favorites)
        item?.isVisible = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun afterTextChanged(p0: Editable) {
        if (p0.isNotEmpty()) {
            if (p0.startsWith('.')) {
                p0.insert(0, "0")
            }
            if (p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))) {
                calculateExchange(input.text.toString().toFloat())
            }
        }else{
            conversionOne.text = "0.00"
            conversionTwo.text = "0.00"
            conversionThree.text = "0.00"
            conversionFour.text = "0.00"
        }
    }

    override fun beforeTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
       if(p0.isNotEmpty()){
           if (p0.startsWith('.')) {
               p0.replaceFirst(Regex("[.]"), "0.")
           }
           if (p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))) {
               calculateExchange(input.text.toString().toFloat())
           }
       }else{
           conversionOne.text = "0.00"
           conversionTwo.text = "0.00"
           conversionThree.text = "0.00"
           conversionFour.text = "0.00"
       }


    }

    override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
        if(p0.isNotEmpty()){
            if (p0.startsWith('.')) {
                p0.replaceFirst(Regex("[.]"), "0.")
            }
            if (p0.toString().isNotEmpty() && !(p0.toString().endsWith('.'))) {
                calculateExchange(input.text.toString().toFloat())
            }
        }else{
            conversionOne.text = "0.00"
            conversionTwo.text = "0.00"
            conversionThree.text = "0.00"
            conversionFour.text = "0.00"
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.favorites -> {
                val dialog = FavoritesDialog.newInstance(this)
                dialog.show(fragmentManager, "favorites")

            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setTextViews() {
        if (favRates.isNotEmpty()) {
            countryOne.text = favRates[0].country
            countryTwo.text = favRates[1].country
            countryThree.text = favRates[2].country
            countryFour.text = favRates[3].country
        }
    }

    private fun calculateExchange(money : Float) {
        if (favRates.isNotEmpty()) {
           conversionOne.text = String.format("%.3f",money /favRates[0].buy)
           conversionTwo.text = String.format("%.3f",money /favRates[1].buy)
           conversionThree.text = String.format("%.3f",money /favRates[2].buy)
           conversionFour.text = String.format("%.3f",money /favRates[3].buy)
        }
    }

    override fun saveFavorites(rates: ArrayList<Currency>) {
        favRates = rates
        setTextViews()
        calculateExchange(input.text.toString().toFloat())
    }
}// Required empty public constructor
