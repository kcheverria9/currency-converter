package android.kc.currencyconverter


import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.kc.currencyconverter.Helper.MySQLiteHelper
import android.kc.currencyconverter.Model.Currency
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.splash_screen.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.IOException
import java.lang.Float
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SplashScreen : AppCompatActivity() {

    companion object {
        var currencies: ArrayList<Currency> = ArrayList()
        var topCurrencies: ArrayList<Currency> = ArrayList()
        var favRates: ArrayList<Currency> = ArrayList()
        val TOP_CURRENCIES = "http://www.boj.org.jm/foreign_exchange/fx_crates.php"
        val ALL_CURRENCIES = "http://www.boj.org.jm/foreign_exchange/fx_irates.php"
        var defaultCurrency = Currency()
    }

    lateinit var realm: Realm
    var allCurrenciesRan = false


    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
        realm = Realm.getDefaultInstance()
        if (!(realm.isEmpty)) {
            defaultCurrency = realm.where<Currency>().findFirst()!!
        }else{
            defaultCurrency.country = "Jamaica"
            defaultCurrency.buy = 1.00f
            defaultCurrency.sell = 1.00f
            defaultCurrency.recentDate = SimpleDateFormat("yyyy-MM-dd", Locale.US).toLocalizedPattern()
            defaultCurrency.websiteDate = SimpleDateFormat("yyyy-MM-dd", Locale.US).toLocalizedPattern()
        }
        loading_bar.progress = 0
        loading_bar.isIndeterminate = true
        loading_bar.indeterminateDrawable.setColorFilter(Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN)

        val db = MySQLiteHelper(this)
        val currentDate = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time).toString()
        val mostRecent = db.mostRecentDate
        val topMostRecentDate = db.topMostRecentDate
        if (mostRecent == currentDate && topMostRecentDate == currentDate) {
            currencies = db.allRates
            topCurrencies = db.allTopRates
            favRates = db.favRates
            val intent = Intent(this@SplashScreen, RatesActivity::class.java)
            intent.putExtra("allCurrencies", currencies)
            intent.putExtra("topCurrencies", topCurrencies)
            intent.putExtra("favorites", favRates)
            startActivity(intent)
            finish()
        } else {
            Currencies().execute(TOP_CURRENCIES)
            Currencies().execute(ALL_CURRENCIES)
        }

    }

    @SuppressLint("StaticFieldLeak")
    inner class Currencies : AsyncTask<String, Int, Void>() {

        @SuppressLint("SimpleDateFormat")
        override fun doInBackground(vararg p0: String?): Void? {
            val document: Document?
            val db = MySQLiteHelper(this@SplashScreen)
            try {
                if (p0[0] == ALL_CURRENCIES) {
                    allCurrenciesRan = true
                }
                val response = Jsoup.connect(p0[0]).execute()
                val htmlDocument = response.body()
                document = Jsoup.parse(htmlDocument)
                if (document != null) {
                    val tableBody = document.getElementsByTag("tbody")
                    when (p0[0]) {
                        SplashScreen.ALL_CURRENCIES -> db.clearAllRates()
                        SplashScreen.TOP_CURRENCIES -> db.clearTopRates()
                    }

                    tableBody.forEach { table ->
                        val currencyRow = table.getElementsByTag("tr")
                        currencyRow.forEach { row ->
                            val cells = row.getElementsByTag("td")
                            when (p0[0]) {
                                ALL_CURRENCIES -> {
                                    var buy = Float.parseFloat(cells[1].text().replace("[^\\d.]", ""))
                                    var sell = Float.parseFloat(cells[2].text().replace("[^\\d.]", ""))
                                    val date = document.getElementsByTag("strong")[0].text()
                                    val recentDate = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)
//                                    val currency = Currency(cells[0].text(), buy, sell, recentDate.toString(), date)
                                    buy /= defaultCurrency.buy
                                    sell /= defaultCurrency.sell
                                    val currency = Currency(cells[0].text(), buy, sell, recentDate.toString(), date)
                                    currencies.add(currency)
                                    db.addAllRate(currency)
                                }
                                TOP_CURRENCIES -> {
                                    var buy = Float.parseFloat(cells[1].text().replace("[^\\d.]", ""))
                                    var sell = Float.parseFloat(cells[4].text().replace("[^\\d.]", ""))
                                    val date = document.getElementsByTag("b")[1].text()
                                    val recentDate = SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().time)
                                    buy /= defaultCurrency.buy
                                    sell /= defaultCurrency.sell
                                    val currency = Currency(cells[0].text(), buy, sell, recentDate.toString(), date)
                                    topCurrencies.add(currency)
                                    db.addTopRate(currency)
                                }
                            }
                        }
                    }
                }
            } catch (exception: IOException) {
                exception.printStackTrace()
                runOnUiThread({
                    Toast.makeText(this@SplashScreen, "Connection failure", Toast.LENGTH_SHORT).show()
                    if (topCurrencies.isEmpty()) {
                        topCurrencies = db.allTopRates
                    } else {
                        currencies = db.allRates
                    }
                })
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            if (allCurrenciesRan) {
                val db = MySQLiteHelper(this@SplashScreen)
                val intent = Intent(this@SplashScreen, RatesActivity::class.java)
                favRates = db.favRates
                intent.putExtra("allCurrencies", currencies)
                intent.putExtra("topCurrencies", topCurrencies)
                intent.putExtra("favorites", favRates)
                intent.putExtra("default", defaultCurrency)
                startActivity(intent)
                finish()
            }
        }
    }
}