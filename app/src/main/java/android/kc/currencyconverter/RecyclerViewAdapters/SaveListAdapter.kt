package android.kc.currencyconverter.RecyclerViewAdapters

import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.R
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class SaveListAdapter(var list: ArrayList<Currency>) : RecyclerView.Adapter<SaveListAdapter.ListHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.save_list, parent, false)
        return ListHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ListHolder, position: Int) {
        holder.countryName.text = list[position].country
    }

    class ListHolder(country: View) : ViewHolder(country) {
        var view : View = country
        var countryName : TextView = view.findViewById(R.id.currency_country)

    }
}