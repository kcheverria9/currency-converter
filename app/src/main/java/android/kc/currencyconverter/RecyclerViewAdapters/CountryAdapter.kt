package android.kc.currencyconverter.RecyclerViewAdapters

import android.kc.currencyconverter.Interfaces.CurrencyClickListener
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.R
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


class CountryAdapter(private val countries: ArrayList<Currency>, val currencyClickListener: CurrencyClickListener) : RecyclerView.Adapter<CountryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.conversions_recyclerview, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.countryName.text = countries[position].country
        holder.sellingRate.text = String.format("Selling: $%.2f", countries[position].sell)
        holder.buyingRate.text = String.format("Buying: $%.2f", countries[position].buy)
        holder.view.setOnClickListener({
            currencyClickListener.onCurrencyClicked(countries[position])
        })
    }

    override fun getItemCount() = countries.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view: View = itemView
        var countryName: TextView = itemView.findViewById(R.id.country_name)
        var sellingRate: TextView = itemView.findViewById(R.id.selling_rate)
        var buyingRate: TextView = itemView.findViewById(R.id.buying_rate)
    }


}