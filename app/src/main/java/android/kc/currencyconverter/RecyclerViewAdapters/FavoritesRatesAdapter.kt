package android.kc.currencyconverter.RecyclerViewAdapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ObservableArrayList
import android.databinding.ViewDataBinding
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.R
import android.kc.currencyconverter.databinding.SelectedRatesItemBinding
import android.kc.currencyconverter.databinding.UnselectedRatesItemBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


class FavoritesRatesAdapter(val view: RecyclerView, val list: ObservableArrayList<Currency>, val layout: Int) : RecyclerView.Adapter<FavoritesRatesAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        context = parent.context
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currency = list[position]
        when (layout) {
            R.layout.selected_rates_layout -> {
                val selectedRatesItemBinding = holder.dataBinding as SelectedRatesItemBinding
                selectedRatesItemBinding.currency = currency
                selectedRatesItemBinding.favoriteRatesAdapter = this
            }
            R.layout.unselected_rates_layout -> {
                val unselectedRatesLayoutBinding = holder.dataBinding as UnselectedRatesItemBinding
                unselectedRatesLayoutBinding.currency = currency
                unselectedRatesLayoutBinding.favoriteRatesAdapter = this
            }
        }
        holder.dataBinding!!.executePendingBindings()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dataBinding = DataBindingUtil.bind<ViewDataBinding>(itemView)
    }
}