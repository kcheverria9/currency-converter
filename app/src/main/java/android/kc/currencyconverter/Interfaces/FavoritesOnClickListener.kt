package android.kc.currencyconverter.Interfaces

import android.kc.currencyconverter.Model.Currency


interface FavoritesSaveListener {
    fun saveFavorites(rates : ArrayList<Currency>)
}