package android.kc.currencyconverter.Interfaces

import android.kc.currencyconverter.Model.Currency


interface CurrencyClickListener {
    fun onCurrencyClicked(currency: Currency)
}