package android.kc.currencyconverter.KotlinExtensions

import android.databinding.ObservableArrayList
import android.kc.currencyconverter.Model.Currency
import android.view.MotionEvent
import android.view.View

fun ArrayList<Currency>.toObservableArrayList() : ObservableArrayList<Currency>{
    val rates : ObservableArrayList<Currency> = ObservableArrayList()
    return this.mapTo(rates){it}
}



fun ArrayList<Currency>.findByName(string: String): Currency {
    this.filter { it.country == string }
            .forEach { return it }
    return Currency()
}

/*fun MotionEvent.getView():View{
    val view : View
    val x = this.x
    val y = this.y
    this
}*/


//compares the top rates and the favorites and takes out what already exists in favorites out of top rates
fun ArrayList<Currency>.removeFavorites(list: ArrayList<Currency>) : ObservableArrayList<Currency>{
    val rates : ObservableArrayList<Currency> = ObservableArrayList()
    list.filter { it !in this}.mapTo(rates){it}
    return rates
}

