@file:Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN", "DEPRECATION")

package android.kc.currencyconverter

import android.annotation.SuppressLint
import android.content.Intent
import android.kc.currencyconverter.Fragments.BuyingFragment
import android.kc.currencyconverter.Fragments.FavoritesFragment
import android.kc.currencyconverter.Fragments.RatesFragment
import android.kc.currencyconverter.Fragments.SellingFragment
import android.kc.currencyconverter.Model.Currency
import android.kc.currencyconverter.Services.RatesService
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import io.realm.Realm
import io.realm.kotlin.createObject
import kotlinx.android.synthetic.main.activity_rates.*
import kotlinx.android.synthetic.main.app_bar_rates.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates


@Suppress("UNCHECKED_CAST")
class RatesActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        var currencies: ArrayList<Currency> = ArrayList()
        var topCurrencies: ArrayList<Currency> = ArrayList()
        var favRates: ArrayList<Currency> = ArrayList()
        var defaultCurrency: Currency? = null
    }

    private var realm: Realm by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rates)
        setSupportActionBar(toolbar)
        realm = Realm.getDefaultInstance()
        currencies = intent.getSerializableExtra("allCurrencies") as ArrayList<Currency>
        topCurrencies = intent.getSerializableExtra("topCurrencies") as ArrayList<Currency>
        favRates = intent.getSerializableExtra("favorites") as ArrayList<Currency>
        if (realm.isEmpty) {
            Toast.makeText(this, "Default will be set to Jamaican.", Toast.LENGTH_SHORT).show()
            realm.executeTransaction {
                val currency = realm.createObject<Currency>(1)
                currency.country = "Jamaica"
                currency.buy = 1.00f
                currency.sell = 1.00f
                currency.recentDate = SimpleDateFormat("yyyy-MM-dd", Locale.US).toLocalizedPattern()
                currency.websiteDate = SimpleDateFormat("yyyy-MM-dd", Locale.US).toLocalizedPattern()
            }
        }
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        nav_view.menu.getItem(0).isChecked = true
        nav_view.menu.performIdentifierAction(R.id.fav_currencies, 0)
        toolbar.title = "Favorites"
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        realm.close()
        startService(Intent(this, RatesService::class.java))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.rates, menu)
        return true
    }

    @SuppressLint("CommitTransaction")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.fav_currencies -> {
                val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.frame_layout, FavoritesFragment(), "FAVS")
                transaction.commit()
                this.toolbar.title = "Favorites"
            }
            R.id.top_currencies -> {
                val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.frame_layout, RatesFragment(), "RATES")
                transaction.commit()
                this.toolbar.title = "Rates"
            }
            R.id.buy_currency -> {
                val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                val buy = BuyingFragment()
                val bundle = Bundle()
                bundle.putSerializable("buy", currencies)
                buy.arguments = bundle
                transaction.replace(R.id.frame_layout, buy, "BUY")
                transaction.commit()
                this.toolbar.title = "Buy"
            }
            R.id.sell_currency -> {
                val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                val sell = SellingFragment()
                val bundle = Bundle()
                bundle.putSerializable("sell", currencies)
                sell.arguments = bundle
                transaction.replace(R.id.frame_layout, sell, "SELL")
                transaction.commit()
                this.toolbar.title = "Sell"
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
